import 'package:flutter/material.dart';
import 'package:learning_flutter_exercise_2/generated/i18n.dart';
import 'package:learning_flutter_exercise_2/bloc/comment_bloc.dart';
import 'package:learning_flutter_exercise_2/model/comment.dart';
import 'package:learning_flutter_exercise_2/model/post.dart';
import 'package:learning_flutter_exercise_2/repository/network/api_response.dart';
import 'package:learning_flutter_exercise_2/utils/utils.dart';
import 'package:learning_flutter_exercise_2/utils/firebase_service.dart';

class PostDetail extends StatefulWidget {
  final Post post;
  final String currentScreen = "post_detail_page";

  PostDetail({Key key, this.post}) : super(key: key) {
    firebaseService.trackScreenView(currentScreen);
  }

  @override
  _PostDetailState createState() => _PostDetailState();
}

class _PostDetailState extends State<PostDetail> {
  CommentBloc commBloc;

  @override
  void initState() {
    commBloc = CommentBloc(widget.post.id ?? 0);
    super.initState();
  }

  @override
  void dispose() {
    commBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        firebaseService.trackButtonClick(widget.currentScreen, "back_button");
        return true;
      },
      child: Scaffold(
        appBar: AppBar(title: Text(S.of(context).post_details)),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _post(),
                _comments(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _post() {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                child: Text(widget?.post?.title ?? "", style: ThemeText.title),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 20.0),
                child:
                    Text(widget?.post?.body ?? "", style: ThemeText.postBody),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _comments(context) {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 8.0),
                child: Text(
                  S.of(context).comments,
                  style: ThemeText.header,
                ),
              ),
              Flexible(
                child: _getComments(),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _getComments() {
    return StreamBuilder<ApiResponse<List<Comment>>>(
      stream: commBloc.commentsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          switch (snapshot.data.status) {
            case Status.LOADING:
              return Center(child: CircularProgressIndicator());
              break;
            case Status.ERROR:
              return Text("ERROR: " + snapshot.data.message);
              break;
            case Status.COMPLETED:
              return CommentsList(commentsList: snapshot.data.data);
              break;
          }
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}

class CommentsList extends StatelessWidget {
  final List<Comment> commentsList;

  const CommentsList({Key key, this.commentsList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: ListView.builder(
                physics: const ScrollPhysics(),
                shrinkWrap: true,
                itemCount: commentsList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          "${commentsList[index]?.name ?? ""}",
                          style: ThemeText.commentName,
                        ),
                        Text(
                          "${commentsList[index]?.email ?? ""}",
                          style: ThemeText.commentEmail,
                        ),
                      ],
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5.0, 0, 18.0),
                      child: Text("${commentsList[index]?.body ?? ""}",
                          style: ThemeText.commentBody),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
