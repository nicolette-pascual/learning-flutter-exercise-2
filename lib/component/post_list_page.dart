import 'package:flutter/material.dart';
import 'package:learning_flutter_exercise_2/generated/i18n.dart';
import 'package:learning_flutter_exercise_2/bloc/post_bloc.dart';
import 'package:learning_flutter_exercise_2/component/post_create_page.dart';
import 'package:learning_flutter_exercise_2/component/post_detail_page.dart';
import 'package:learning_flutter_exercise_2/model/post.dart';
import 'package:learning_flutter_exercise_2/repository/network/api_response.dart';
import 'package:learning_flutter_exercise_2/utils/firebase_service.dart';

class MyHomePage extends StatefulWidget {
  String currentScreen;

  MyHomePage({Key key, this.currentScreen}) : super(key: key) {
    currentScreen = "post_list_page";
    firebaseService.trackScreenView(currentScreen);
  }

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text(S.of(context).app_title)),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                firebaseService.trackButtonClick(
                    widget.currentScreen, "create_new_post_button");
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        settings: RouteSettings(name: 'post_create_page'),
                        builder: (BuildContext context) {
                          return PostCreate();
                        }));
              },
            )
          ],
        ),
        body: Container(
          child: StreamBuilder<ApiResponse<List<Post>>>(
              stream: bloc.postListStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  switch (snapshot.data.status) {
                    case Status.LOADING:
                      return Loading(loadingMessage: snapshot.data.message);
                      break;
                    case Status.ERROR:
                      return Text(snapshot.data.message);
                      break;
                    case Status.COMPLETED:
                      return PostList(
                          postList: snapshot.data.data,
                          currentScreen: widget.currentScreen);
                      break;
                  }
                }
                return Center(child: CircularProgressIndicator());
              }),
        ));
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;
  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(loadingMessage, textAlign: TextAlign.center),
          CircularProgressIndicator()
        ],
      ),
    );
  }
}

class PostList extends StatelessWidget {
  final List<Post> postList;
  final String currentScreen;

  const PostList({Key key, this.postList, this.currentScreen})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _postListSection(postList),
          _showMoreButton(context, postList)
        ],
      ),
    );
  }

  Widget _postListSection(postList) {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: ListView.separated(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  separatorBuilder: (context, int index) => Divider(
                    thickness: 1.0,
                  ),
                  itemCount: postList.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text("${postList[index]?.title ?? ""}"),
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              settings:
                                  RouteSettings(name: 'post_details_page'),
                              builder: (context) {
                                firebaseService.trackViewItem(
                                    postList[index].id.toString(),
                                    postList[index].title);
                                return PostDetail(post: postList[index]);
                              })),
                    );
                  },
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _showMoreButton(context, postList) {
    return Visibility(
      visible:
          bloc.getDataLength() == bloc.getDisplayDataLength() ? false : true,
      child: Row(
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                FlatButton(
                  color: Colors.blue,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  child: Text(S.of(context).show_more),
                  onPressed: () {
                    firebaseService.trackButtonClick(
                        currentScreen, "show_more_button");
                    bloc.showMoreListSink.add(ApiResponse.completed(postList));
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
