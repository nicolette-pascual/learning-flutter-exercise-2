import 'package:flutter/material.dart';
import 'package:learning_flutter_exercise_2/generated/i18n.dart';
import 'package:learning_flutter_exercise_2/repository/post_repository.dart';
import 'package:learning_flutter_exercise_2/utils/firebase_service.dart';

class PostCreate extends StatelessWidget {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descController = TextEditingController();
  String _title = "", _desc = "";
  PostRepository _postRepository = PostRepository();
  final String currentScreen = "post_create_page";
  final _key = GlobalKey<FormState>();

  PostCreate() {
    firebaseService.trackScreenView(currentScreen);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        firebaseService.trackButtonClick(currentScreen, "back_button");
        return true;
      },
      child: Scaffold(
          appBar: AppBar(title: Center(child: Text(S.of(context).new_post))),
          body: Form(
            key: _key,
            child: _newPostFormSection(context),
          )),
    );
  }

  Widget _newPostFormSection(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        TextFormField(
          controller: _titleController,
          decoration: InputDecoration(labelText: S.of(context).title),
          validator: (value) =>
              value.isEmpty ? S.of(context).please_enter_a_title : null,
        ),
        TextFormField(
          controller: _descController,
          decoration: InputDecoration(labelText: S.of(context).description),
          validator: (value) =>
              value.isEmpty ? S.of(context).please_enter_a_description : null,
        ),
        Flexible(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: ButtonTheme(
              minWidth: double.infinity,
              child: RaisedButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                color: Colors.blue,
                child: Text(S.of(context).submit),
                onPressed: () {
                  _onSubmitPressed(context);
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  void _onSubmitPressed(context) async {
    if (_key.currentState.validate()) {
      _title = _titleController.text;
      _desc = _descController.text;
      await _postRepository.createPost(1, _title, _desc);
      firebaseService.trackButtonClick(currentScreen, "submit_button");
      firebaseService.trackFormSubmit(currentScreen, "new_post_form");
      Navigator.pop(context);
    }
  }
}
