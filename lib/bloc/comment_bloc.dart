import 'dart:async';
import 'package:learning_flutter_exercise_2/model/comment.dart';
import 'package:learning_flutter_exercise_2/repository/comment_repository.dart';
import 'package:learning_flutter_exercise_2/repository/network/api_response.dart';
import 'package:rxdart/rxdart.dart';

class CommentBloc {
  CommentRepository _commentRepository;
  List<Comment> _data = List<Comment>();

  final _commentsStreamController =
      BehaviorSubject<ApiResponse<List<Comment>>>();

  Stream<ApiResponse<List<Comment>>> get commentsStream =>
      _commentsStreamController.stream;
  StreamSink<ApiResponse<List<Comment>>> get commentsSink =>
      _commentsStreamController.sink;

  CommentBloc(int postId) {
    _commentRepository = CommentRepository();
    _init(postId);
  }

  void _init(postId) async {
    commentsSink.add(ApiResponse.loading('Fetching data...'));
    try {
      _data = await _commentRepository.fetchAllComments(postId);
      _commentsStreamController.add(ApiResponse.completed(_data));
    } catch (e) {
      commentsSink.add(ApiResponse.error(e.toString()));
      print("ERROR: " + e.toString());
    }
  }

  void dispose() {
    _commentsStreamController.close();
  }
}
