import 'dart:async';
import 'package:learning_flutter_exercise_2/model/post.dart';
import 'package:learning_flutter_exercise_2/repository/network/api_response.dart';
import 'package:learning_flutter_exercise_2/repository/post_repository.dart';
import 'package:rxdart/rxdart.dart';

class PostBloc {
  static const int _start = 0;
  final int _morePosts = 5;
  int _perPage = 5;

  List<Post> _data = List<Post>();
  List<Post> _displayData = List<Post>();

  PostRepository _postRepository;

  int getPerPage() => _perPage;
  int getDisplayDataLength() => _displayData.length ?? 0;
  int getDataLength() => _data.length ?? 0;

  final _postListStreamController = PublishSubject<ApiResponse<List<Post>>>();
  final _showMoreListStreamController =
      PublishSubject<ApiResponse<List<Post>>>();

  Stream<ApiResponse<List<Post>>> get postListStream =>
      _postListStreamController.stream;
  StreamSink<ApiResponse<List<Post>>> get postListSink =>
      _postListStreamController.sink;

  StreamSink<ApiResponse<List<Post>>> get showMoreListSink =>
      _showMoreListStreamController.sink;

  PostBloc() {
    _postRepository = PostRepository();
    _init();
  }

  void _init() async {
    postListSink.add(ApiResponse.loading('Fetching data...'));
    try {
      _data = await _postRepository.fetchPostList();
      _displayData = [...?_data.getRange(_start, _perPage)];
      _postListStreamController.add(ApiResponse.completed(_displayData));
      _showMoreListStreamController.stream.listen(_showMorePosts);
    } catch (e) {
      postListSink.add(ApiResponse.error(e.toString()));
      print("ERROR: " + e.toString());
    }
  }

  void _showMorePosts(ApiResponse<List<Post>> post) {
    _perPage += _morePosts;
    if (_perPage < _data.length) {
      _displayData.clear();
      _displayData = [...?_data.getRange(_start, _perPage)];
      postListSink.add(ApiResponse.completed(_displayData));
    }
  }

  void dispose() {
    _postListStreamController?.close();
    _showMoreListStreamController.close();
  }
}

final bloc = PostBloc();
