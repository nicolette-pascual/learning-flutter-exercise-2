import 'dart:async';

import 'package:flutter/material.dart';
import 'generated/i18n.dart';
import 'component/post_list_page.dart';

import 'utils/firebase_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  firebaseService.init();
  runZoned<Future<Null>>(() async {
    runApp(MyApp());
  }, onError: (error, stackTrace) async {
    await firebaseService.logCrash(error, stackTrace);
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorObservers: <NavigatorObserver>[firebaseService.observer],
      localizationsDelegates: [S.delegate],
      supportedLocales: S.delegate.supportedLocales,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
      onGenerateTitle: (context) => S.of(context).app_title,
    );
  }
}
