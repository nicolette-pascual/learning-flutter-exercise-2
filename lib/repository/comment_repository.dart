import 'package:learning_flutter_exercise_2/model/comment.dart';

import 'network/api_base_helper.dart';

class CommentRepository {
  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<Comment>> fetchAllComments(int i) async {
    final response = await _helper.get("comments?postId=" + i.toString());
    List<Comment> _comments =
        response.map((json) => Comment.fromJson(json)).toList().cast<Comment>();

    return _comments;
  }
}
