import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:learning_flutter_exercise_2/model/post.dart';
import 'package:learning_flutter_exercise_2/utils/firebase_service.dart';

import 'custom_exception.dart';

class ApiBaseHelper {
  final String _baseUrl = "https://jsonplaceholder.typicode.com/";

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      firebaseService.trackApiEvent(
          FirebaseService.API_SEND_REQUEST, _baseUrl + url);
      final response = await http.get(_baseUrl + url);
      responseJson = _returnResponse(response, _baseUrl + url);
    } on SocketException {
      firebaseService.trackApiEvent(
          FirebaseService.API_FAILED_REQUEST, _baseUrl + url,
          error: 'No Internet Connection');
      throw FetchDataException('No Internet Connection');
    } catch (e) {
      firebaseService.trackApiEvent(
          FirebaseService.API_FAILED_REQUEST, _baseUrl + url,
          error: e.toString());
      firebaseService.logErrorSilently(e);
    }
    return responseJson;
  }

  Future<dynamic> post(String url, int id, String title, String body) async {
    Map<String, String> _headers = {"Content-type": "application/json"};
    Post _post = Post(userId: id, title: title, body: body);
    String _json = jsonEncode(_post);
    var responseJson;
    try {
      firebaseService.trackApiEvent(
          FirebaseService.API_SEND_REQUEST, _baseUrl + url);
      final response =
          await http.post(_baseUrl + url, headers: _headers, body: _json);
      responseJson = _returnResponse(response, _baseUrl + url);
    } on SocketException {
      firebaseService.trackApiEvent(
          FirebaseService.API_FAILED_REQUEST, _baseUrl + url,
          error: 'No Internet Connection');
      throw FetchDataException('No Internet Connection');
    } catch (e) {
      firebaseService.trackApiEvent(
          FirebaseService.API_FAILED_REQUEST, _baseUrl + url,
          error: e.toString());
      firebaseService.logErrorSilently(e);
    }
  }

  dynamic _returnResponse(http.Response response, String url) {
    response.statusCode == 200 || response.statusCode == 201
        ? firebaseService.trackApiEvent(
            FirebaseService.API_RECEIVED_RESPONSE, response.request.toString(),
            statusCode: response.statusCode.toString())
        : firebaseService.trackApiEvent(
            FirebaseService.API_FAILED_RESPONSE, response.request.toString(),
            statusCode: response.statusCode.toString(),
            error: response.body.toString());

    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 201:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
