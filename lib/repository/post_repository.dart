import 'package:learning_flutter_exercise_2/model/post.dart';
import 'package:learning_flutter_exercise_2/utils/firebase_service.dart';
import 'network/api_base_helper.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class PostRepository {
  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<Post>> fetchPostList() async {
    final response = await _helper.get("posts/");
    List<Post> _posts =
        response.map((json) => Post.fromJson(json)).toList().cast<Post>();
    return _posts;
  }

  createPost(int id, String title, String body) async {
    final _response = await _helper.post("posts/", id, title, body);
    return _response;
  }
}
