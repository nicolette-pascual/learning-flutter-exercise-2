import 'dart:async';

import 'package:firebase_analytics/observer.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:flutter/material.dart';

/// Handles initialization of Firebase services/plugins,
/// contains methods that will log events to Analytics,
/// and crashes/non-fatal errors to Crashlytics
///
/// [logEvent] function is used to manually log a custom event to Analytics
/// and can optionally log an event with parameters
class FirebaseService {
  FirebaseAnalytics _analytics;
  FlutterCrashlytics _crashlytics;

  FirebaseService() {
    _analytics = FirebaseAnalytics();
    _crashlytics = FlutterCrashlytics();
  }

  /// The object for FirebaseAnalyticsObserver for Flutter NavigatorObserver to
  /// track PageRoute
  FirebaseAnalyticsObserver get observer =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  /// Event names for [trackApiEvent()]
  static String API_SEND_REQUEST = "send_request";
  static String API_FAILED_REQUEST = "failed_request";
  static String API_RECEIVED_RESPONSE = "received_response";
  static String API_FAILED_RESPONSE = "failed_response";

  /// Initializes the use of Flutter Crashlytics plugin for the entire app
  /// [ref link]: https://pub.dev/packages/flutter_crashlytics
  void init() async {
    bool isInDebugMode = false;
    FlutterError.onError = (FlutterErrorDetails details) {
      if (isInDebugMode) {
        // In development mode, simply print to console
        FlutterError.dumpErrorToConsole(details);
      } else {
        // In production mode, report to the application zone to report to
        // Crashlytics
        Zone.current.handleUncaughtError(details.exception, details.stack);
      }
    };
    await _crashlytics.initialize();
    // Calls method to log the event of initialization to Firebase Analytics
    trackServiceInit("firebase");
  }

  /// Logs the current screen view to Analytics
  void trackScreenView(screenName) async {
    // Sets the current screen name for when Firebase Analytics automatically
    // logs a screen_view event
    await _analytics.setCurrentScreen(screenName: screenName);
    // Manually logs an Analytics event with the current screen name
    await _analytics.logEvent(name: screenName);
  }

  /// Logs an API event for Analytics
  ///
  /// Takes optional parameters of [statusCode] and/or [error]
  /// if the method will log a failed request and/or response
  void trackApiEvent(eventName, endpoint,
      {String statusCode = "", String error = ""}) async {
    await _analytics.logEvent(name: eventName, parameters: <String, String>{
      'endpoint': endpoint,
      'status_code': statusCode,
      'error': error
    });
  }

  /// Logs a button click event to Analytics
  void trackButtonClick(screenName, buttonName) async {
    await _analytics.logEvent(
        name: buttonName,
        parameters: <String, String>{'current_screen': screenName});
  }

  /// Logs an event to Analytics when a form is successfully submitted
  void trackFormSubmit(screenName, formName) async {
    await _analytics.logEvent(
        name: formName,
        parameters: <String, String>{'current_screen': screenName});
  }

  /// Logs an event to Analytics when a dialog is shown
  void trackDialogShow(screenName, dialogName) async {
    await _analytics.logEvent(
        name: dialogName,
        parameters: <String, String>{"current_screen": screenName});
  }

  /// Logs an event to Analytics when a button in a dialog is clicked
  void trackDialogButtonClick(screenName, dialogName, buttonName) async {
    await _analytics.logEvent(name: dialogName, parameters: <String, String>{
      "current_screen": screenName,
      "button": buttonName
    });
  }

  /// Logs an event to Analytics that some content was shown to the user
  ///
  /// [logViewItem] function takes three parameters
  void trackViewItem(id, name) async {
    await _analytics.logViewItem(
        itemId: id, itemName: name, itemCategory: null);
  }

  /// Logs a Login event to Analytics
  ///
  /// Could take a parameter [logInMethod] that takes a String naming
  /// the method used to log in
  void trackUserLogin() async {
    await _analytics.logLogin();
  }

  /// Logs the log out event of a user to Analytics
  void trackUserLogOut(screenName, userId) async {
    await _analytics.logEvent(name: "log_out", parameters: <String, String>{
      "current_screen": screenName,
      "user_id": userId
    });
  }

  /// Logs a Login event to Analytics
  ///
  /// Could take a parameter [signUpMethod] that takes
  /// a String naming the method used to sign up
  /// For example, "facebook" or "google"
  void trackUserSignUp() async {
    await _analytics.logSignUp();
  }

  /// Logs the initialization event of a service to Analytics
  void trackServiceInit(service) async {
    await _analytics.logEvent(
        name: "service_initiated",
        parameters: <String, String>{"service": service});
  }

  /// Logs an event to Analytics when a service is stopped
  void trackServiceStopped(service) async {
    await _analytics.logEvent(
        name: "service_initiated",
        parameters: <String, String>{"service": service});
  }

  /// Logs an event to Analytics when an error message is shown
  void trackErrorMessageShown(errorMessage) async {
    await _analytics.logEvent(name: errorMessage);
  }

  /// Manually records non-fatal exceptions to Crashlytics
  void logErrorSilently(error) async {
    await _crashlytics.logException(error, error.stackTrace);
  }

  /// Sends errors to Crashlytics
  ///
  /// [forceCrash] allows to have a real crash instead of the red screen,
  /// tagging the exception as fatal
  Future<void> logCrash(error, stackTrace) {
    return _crashlytics.reportCrash(error, stackTrace, forceCrash: true);
  }
}

final FirebaseService firebaseService = FirebaseService();
