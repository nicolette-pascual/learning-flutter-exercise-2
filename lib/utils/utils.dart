import 'package:flutter/material.dart';

abstract class ThemeText {
  static const TextStyle title =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0);

  static const TextStyle header = TextStyle(fontSize: 20.0);

  static const TextStyle postBody = TextStyle(fontSize: 18.0);

  static const TextStyle commentName =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0);

  static const TextStyle commentEmail = TextStyle(color: Colors.grey);

  static const TextStyle commentBody = TextStyle(color: Colors.black);
}
